plugins {
    // Apply the Kotlin JVM plugin to add support for Kotlin on the JVM.
    id("org.jetbrains.kotlin.jvm").version("1.3.31")

    // Apply the application plugin to add support for building a CLI application.
    application

    // publish artifacts
    id("java")
    id("maven-publish")
}

group = "de.syna.hoechstdigital.msa"
version = if (version != "unspecified") version else "0.0.4911-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
    mavenCentral()
    maven { url = uri("https://repo.spring.io/milestone") }
}

dependencies {
    // Use the Kotlin JDK 8 standard library.
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // Use the Kotlin test library.
    testImplementation("org.jetbrains.kotlin:kotlin-test")

    // Use the Kotlin JUnit integration.
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
}

application {
    // Define the main class for the application
    mainClassName = "de.syna.hoechstdigital.msa.proxy.suewagonlineservice.AppKt"
}

publishing {
    publications {
        create<MavenPublication>("java") {
            from(components["java"])
        }
    }

    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/xxx/packages/maven")
            credentials(HttpHeaderCredentials::class.java) {
                name = "Deploy-Token"
                value = "xxx"
            }
            authentication {
                val header by registering(HttpHeaderAuthentication::class)
            }
        }
    }
}